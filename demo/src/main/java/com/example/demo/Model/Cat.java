package com.example.demo.Model;

public class Cat extends Mammal{

    public Cat(String name) {
        super(name);
    }

    public void Greets(){
        System.out.println("meow");
    }

    @Override
    public String toString() {
        return "Cat [Mammal [Animal [name=" + super.getName() + "]]]";
    }
    

}
