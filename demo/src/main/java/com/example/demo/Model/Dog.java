package com.example.demo.Model;

public class Dog extends Mammal{

    public Dog(String name) {
        super(name);
    }

    public void Greets(){
        System.out.println("woooof");
    }

    public void greets(Dog another){
        System.out.println("woooof");
    }

    @Override
    public String toString() {
        return "Dog [Mammal [Animal [name=" + super.getName() + "]]]";
    }

}
