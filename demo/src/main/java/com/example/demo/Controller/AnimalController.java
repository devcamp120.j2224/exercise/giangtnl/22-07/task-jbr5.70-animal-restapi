package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Animal;
import com.example.demo.Model.Cat;
import com.example.demo.Model.Dog;
import com.example.demo.Service.AnimalService;

@RestController
public class AnimalController {
    
    @Autowired
    private AnimalService animalService;

    @GetMapping("/cats")
    public ArrayList<Animal> getDogList(){

        ArrayList<Animal> list = animalService.getAnimalList();

        ArrayList<Animal> catList = new ArrayList<>();

        for (Animal animal : list) {
            if(animal instanceof Cat){
                catList.add(animal);
            }
        }
        return catList ;
    }

    @GetMapping("/dogs")
    public ArrayList<Animal> getCatList(){

        ArrayList<Animal> list = animalService.getAnimalList();

        ArrayList<Animal> dogList = new ArrayList<>();

        for (Animal animal : list) {
            if(animal instanceof Dog){
                dogList.add(animal);
            }
        }
        return dogList ;
    }

}
